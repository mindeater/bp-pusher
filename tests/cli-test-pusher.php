<?php
/*
 * This test suite should be run from the command line once the Pusher plugin is installed
 */

define('TEST_RPC_SERVER','http://xampp.dev/wordpress/xmlrpc.php');
//define('TEST_RPC_SERVER','http://www.whiteprojects.com.au/xmlrpc.php');
require_once 'curl.class.php';

class xmlrpc_client {
    private $url;
    function __construct($url, $autoload=true) {
        $this->url = $url;
        $this->connection = new curl;
        $this->methods = array();
        if ($autoload) {
            $resp = $this->call('system.listMethods', null);
            $this->methods = $resp;
        }
    }
    public function call($method, $params = null) {
        $post = xmlrpc_encode_request($method, $params);
        return xmlrpc_decode($this->connection->post($this->url, $post));
    }
}
header('Content-Type: text/plain');

echo "\nSTART XML-RPC Method calls\n on ".TEST_RPC_SERVER."\n----------------\n\n";
$client = new xmlrpc_client(TEST_RPC_SERVER, true);

// rwc.post_list

$testList = array(
	//'login',
	//'device_status',
	//'posts_list',
	//'posts_add',
	//'comments_list',
	//'comments_add',
	//'avatar_get_url',
	//'avatar_get',
	'android_connect'
);

$user = 'admin';
$pass = 'admin';

//$user = 'ashley';
//$pass = 'Superm@n2011';


$payload = array(
	'login'				=>array($user,$pass),
	'device_status'		=>array($user,$pass,'a91f34cec898260ecbd681f16314ee457610439725b2d198bf06961cf366edf0',1),
	'posts_list'		=>array($user,$pass),
	'posts_add'			=>array($user,$pass,"Test Script Posting to Activity Feed"),
	'comments_list'		=>array($user,$pass,24),
	'comments_add'		=>array($user,$pass,"I am the commentor who comments on Activity Items ?",'69'),
	'avatar_get_url'	=>array($user,$pass),
	'avatar_get'		=>array($user,$pass),
	'android_connect'=>array("++")
);

foreach($testList as $testMethod){
	echo 'Call rwc.'.$testMethod."\n";
	$resp = $client->call('rwc.'.$testMethod, $payload[$testMethod]);
	if(assert(!empty($resp))){
		echo "recieved response\n.....\n".print_r($resp,true)."\n";
	}else{
		echo"EMPTY RETURN".print_r($resp,true)."\n";
	}
}
echo "\n\n\n DONE\n----------------\n\n";


/*
echo "Call rwc.device_status\n";
$resp = $client->call('rwc.device_status', array('admin','admin',"Hey remotely posted!!"));
if(assert(!empty($resp))){
	echo "recieved response\n.....\n";
}


echo "Call rwc.posts_list\n";
$resp = $client->call('rwc.posts_list', array('admin','admin',"Hey remotely posted!!"));
if(assert(!empty($resp))){
	echo "recieved response\n.....\n";
}else{
	echo "Got activity Feed :\n".print_r($resp);
}


echo "Call rwc.posts_add\n";
$resp = $client->call('rwc.posts_add', array('admin','admin',"Hey remotely posted!!"));
if(assert(!empty($resp))){
	echo "recieved response\n.....\n";
}

echo "Call rwc.comments_list\n";
$resp = $client->call('rwc.comments_list', array('admin','admin',"Hey remotely posted!!"));
if(assert(!empty($resp))){
	echo "recieved response\n.....\n";
}

echo "Call rwc.comments_add\n";
$resp = $client->call('rwc.comments_add', array('admin','admin',"Hey remotely posted!!"));
if(assert(!empty($resp))){
	echo "recieved response\n.....\n";
}

echo "Call rwc.avatar_get_url\n";
$resp = $client->call('rwc.avatar_get_url', array('admin','admin',"Hey remotely posted!!"));
if(assert(!empty($resp))){
	echo "recieved response\n.....\n";
}

echo "Call rwc.avatar_get\n";
$resp = $client->call('rwc.avatar_get', array('admin','admin',"Hey remotely posted!!"));
if(assert(!empty($resp))){
	echo "recieved response\n.....\n";
}
*/

//print_r($client);

?>
