<?php

/**
 * Provides the Pusher_Plugin class.
 *
 * @package    RayWhitePushNotifications
 */
/**
 * Provides some standard functionality related to users.
 *
 * @package    RayWhitePushNotifications
 * @subpackage Pusher
 */
//require_once 'XMLRPCExtend.php';

if (!class_exists("Plugin")) {

	class Plugin {

		/**
		 * Create the Plugin, register the hooks and filters
		 *
		 */
		public function __construct () {

			//actions
			add_action('init', array (__CLASS__, 'init'), 0);
			add_action('admin_init', array (__CLASS__, 'admin_init'));
			add_action('admin_menu', array (__CLASS__, 'admin_menu'));
			add_action('bp_activity_add',
					array (__CLASS__, 'push_notification'), 9, 7);

			//AJAX Admin
			add_action('wp_ajax_pusher_js_send_admin_push',
					array(__CLASS__,'push_from_admin'));
			add_action('wp_ajax_pusher_js_retrive_feedback_admin_push',
					array(__CLASS__,'retrive_feedback_from_admin'));
			add_action('wp_ajax_pusher_js_send_multiple_admin_push',
					array(__CLASS__,'push_multiple_from_admin'));

			// filters
			add_filter('xmlrpc_methods',
					array (__CLASS__, 'add_new_xmlrpc_methods'));
		}

		/**
		 * Set up the plugin to operate
		 *
		 */
		public function init () {

		}

		/**
		 * Create Database tables for use by the pusher
		 *
		 */
		public function install () {
			global $wpdb;

			$table_name = $wpdb->prefix . "pusher_devices";
			// reinit $wpdb->query("DROP TABLE IF EXISTS '$table_name'");
			if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
				$sql = "CREATE TABLE " . $table_name . " (
						`pusher_table_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
						`pusher_deviceToken` VARCHAR( 64 ) NOT NULL ,
						`last_access_date` DATE NOT NULL ,
						`should_get_push` BOOL NOT NULL DEFAULT  '1',
						`wp_user_id` INT UNSIGNED NOT NULL,
						`pusher_deviceType` VARCHAR( 64 ) NOT NULL 
						) ENGINE = MYISAM COMMENT =  'Push notification devices';";

				$wpdb->query($sql);
			}

			//@TODO :: make sure the xml-rpc setting is switched on for wordpress
		}

		/**
		 * Plugin Admin Menu
		 */
		function admin_menu () {

			add_options_page('Buddy Press Pusher Settings', 'BP Pusher', 'manage_options', 'bp-pusher', array (__CLASS__, 'admin_options_page'));
		}

		/**
		 * Add the Option page
		 */
		function admin_options_page () {
			require plugin_dir_path(__FILE__) . '../admin/config-page.php';
		}

		function main_settings_text(){
			echo "The main Plugin settings";
		}

		function development_settings_text(){
			echo "The Development settings";
		}

		function deployment_settings_text(){
			echo "The Deployment settings";
		}

		function mode_switch(){
			//echo "|".print_r(get_option('rw_bp_pusher_options'),true)."|";
			$out ='';
			$out.='<input type="radio" name="pusher_mode" value="Development"> Development <input type="radio" name="pusher_mode" value="Deployment"> Deployment(Live)';

			echo $out;
		}
		/**
		 * Initialise the settings
		 */
		function admin_init () {
			register_setting(
					'rw_bp_pusher_options',
					'rw_bp_pusher_options',
					'rw_bp_pusher_options_validate'); // array (__CLASS__,

			add_settings_section(
					'pusher_main',
					'Main Settings',
					array (__CLASS__, 'main_settings_text'),
					'plugin');
			add_settings_section(
					'pusher_mode',
					'Choose Plugin Mode:',
					array (__CLASS__, 'mode_switch'),
					'plugin',
					'pusher_main'
					);

			add_settings_section(
					'pusher_dev',
					'Development Settings',
					array (__CLASS__, 'development_settings_text'),
					'plugin');
			add_settings_section(
					'pusher_dep',
					'Deploment Settings',
					array (__CLASS__, 'deployment_settings_text'),
					'plugin');

			add_settings_field('plugin_text_string', 'Plugin Text Input', 'plugin_setting_string', 'plugin', 'plugin_main');
		}

		/*
		 * The Push Notification
		 */

		function push_notification ($args) {

			$push = new Push();
			$push->setActivity($args);
			$push->notify();
		}

		/*
		 * AJAX METHODS for Admin
		 *
		 * @TODO: use encrypted token to identify admin
		 */
		function push_from_admin(){
			//
			if($_POST['token'] != 'hf'){
				die();
			}
			$push = new Push();
			$push->notifySingleDevice($_POST['device'],$_POST['message']);
			echo "<p>Message sent to ".$_POST['device'].'</p>';
			die();
		}

		function retrive_feedback_from_admin(){
			//

			if($_POST['token'] != 'hf'){
				die();
			}

			$push = new Push();
			$feedback = $push->retriveFeedback();
			echo '<p>'.$feedback.'</p>';
			die();
		}

		function push_multiple_from_admin(){
			if($_POST['token'] != 'hf'){
				die();
			}

			$push = new Push();
			if(is_array($_POST['deviceList'])){
				$response = $push->notifyMultipleDevices($_POST['deviceList'],$_POST['message']);
			}
			echo '<hr />'.$response."<hr />";
			die();
		}

		/*
		 * Filter to add xml-rpc methods to wordpress
		 *
		 * @param array $methods The current List of xml-rpc methods
		 * @return array $methods  The modified List of xml-rpc methods
		 */

		function add_new_xmlrpc_methods ($methods) {
			//print_r($methods);
			/*
			  apns_add(%token_details) Register the Device for receiving Push Notifications
			  posts_list() or ({ since: $date }) Return a list of Nested items posted (limit 50 / transaction)
			  posts_add(%post) Add an item to the Activity Feed
			  comments_list({ post_id:$id) or ({ user_id:$id }) or ({ since:$date })
			  comments_add(%comment)
			  GET url's for avatar/icons
			 */

			$methods['rwc.device_status'] = array ('XMLRPCExtend', 'setDeviceNotificationStatus');
			$methods['rwc.login'] = array ('XMLRPCExtend', 'loginUser');
			$methods['rwc.posts_list'] = array ('XMLRPCExtend', 'getActivityFeed');
			$methods['rwc.posts_add'] = array ('XMLRPCExtend', 'addActivity');

			$methods['rwc.comments_list'] = array ('XMLRPCExtend', 'getComments');
			$methods['rwc.comments_add'] = array ('XMLRPCExtend', 'addComment');

			$methods['rwc.avatar_get_url'] = array ('XMLRPCExtend', 'getAvatarURL');
			$methods['rwc.avatar_get'] = array ('XMLRPCExtend', 'getAvatar');


			$methods['rwc.android_connect'] = array ('XMLRPCExtend', 'androidTest');
			return $methods;
		}

	}

}
?>