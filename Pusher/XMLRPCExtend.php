<?php

/**
 * Provides the XMLRPCExtend class.
 *
 * @package    RayWhitePushNotifications
 */

/**
 * Provides some standard functionality related to users.
 *
 * @package    RayWhitePushNotifications
 * @subpackage Pusher
 */
class XMLRPCExtend {


	/**
	 * Default Contructor
	 *
	 */
	public function XMLRPCExtend(){}

	public function loginUser($args){
		$user = $args[0];
		$pass = $args[1];

		// authenticate the user
		$isUser = Authenticate::isValidUser($user,$pass);
		if(get_class($isUser) == "IXR_Error"){
			//return $isUser;
			return json_encode(array('login_status'=>1));
		}
		return json_encode(array('login_status'=>0));

	}
	/**
	 * Device notification settings.
	 *
	 * @param
	 *
	 * @return BOOL $done
	 */
	public static function setDeviceNotificationStatus ($args) {

		//return $args;
		$user = $args[0];
		$pass = $args[1];
		$deviceId = str_replace(' ','',$args[2]); // strip any spaces
		$receiveNotifications = $args[3];

		// authenticate the user
		$isUser = Authenticate::isValidUser($user,$pass);
		if(get_class($isUser) == "IXR_Error"){
			return $isUser;
		}

		global $wpdb;
		$query = "SELECT * FROM ". $wpdb->prefix."pusher_devices ".
				"WHERE pusher_deviceToken = '".$deviceId."'";

		if(!$wpdb->query($query)){
			$query = "INSERT INTO ". $wpdb->prefix."pusher_devices ".
				"(should_get_push,pusher_deviceToken,wp_user_id) VALUES (".
					$receiveNotifications.", '".$deviceId."',".$isUser->ID.")";
		}else{
			$query = "UPDATE ". $wpdb->prefix."pusher_devices ".
				"SET should_get_push = ".$receiveNotifications." ".
				"WHERE pusher_deviceToken = '".$deviceId."'";
		}
		if(!$wpdb->query($query)){
			return json_encode(array('error_code'=>1));
				//("deviceId"=>$deviceId,"RESULT"=>"NO RECORDS CHANGED"));
		}

		return json_encode(array('error_code'=>0));
			//("deviceId"=>$deviceId,"RESULT"=>"UPDATE SUCCESS"));
	}


	/**
	 * Return comments assosiated with an activity Item
	 *
	 * @param
	 *
	 * @return JSON nested comments List
	 */

	public static function getComments($args){

		$fp = fopen('_comment_log.txt','a');
		fwrite($fp, print_r($args,true)."\n - -".date("F j, Y, g:i a")." - -\n");
		fclose($fp);

		$user = $args[0];
		$pass = $args[1];
		$activityId = (int)$args[2];

		$isUser = Authenticate::isValidUser($user,$pass);
		if(get_class($isUser) == "IXR_Error"){
			return $isUser;
		}

		// get the activity comments
		$ALLOWED_ACTIVITIES	= array('activity_comment');
		$feed = array();
		$count = 0;
		if (bp_has_activities(array('display_comments'=>'stream','action'=>'activity_comment'))) :
			global $activities_template;
//return print_r($activities_template,true);
			foreach($activities_template->activities as $act){
				if((int)$act->item_id == $activityId){
					$id = $act->id;
					$feed[$count]['activity_id'] = $id;
					$feed[$count]['avatar'] = bp_core_fetch_avatar(array('html'=>false, 'item_id'=>$id));
					$feed[$count]['comment'] = $act->content;
					$feed[$count]['display_name'] = $act->display_name;
					$feed[$count]['date_recorded'] = date("F j, Y, g:i a",strtotime($act->date_recorded));
					if(isset($act->children)){
						$feed[$count]['comment_count'] = count($act->children);
					}
					$count++;
				}

				// TODO test this will exit the loop
				if($count == $limit){
					//break;
				}
			}
		endif;
		$json = json_encode($feed);
		if($json){
			return $json ;
		}

		return $feed;



	}
	/**
	 * Return the BudyPress Activity Feed.
	 *
	 * @param
	 *
	 * @return JSON $activityList A Nested List of the the BuddyPress Activity Feed
	 */
	public static function getActivityFeed ($args) {

		$ALLOWED_ACTIVITIES	= array('activity_update','activity_comment');

		$activityList = $args;
		$user = $args[0];
		$pass = $args[1];
		// TODO :: the bp_has_activities call default limits to 20
		// and can be passed args to set limits on what is returned
		if(isset($args[2])){
			$limit = (int)$args[2];
		}else{
			$limit = false;
		}

		// authenticate the user
		$isUser = Authenticate::isValidUser($user,$pass);

		if(get_class($isUser) == "IXR_Error"){
			return $isUser;
		}

		//return "POSTS LIST";

		//wp_set_current_user( $user->ID );
		// get their feed
		//http://codex.buddypress.org/developer-docs/custom-buddypress-loops/the-activity-stream-loop/
		$feed = array();
		$count = 0;
		if (bp_has_activities()) :
			global $activities_template;
		//return "\n-_-_-__-\n".print_r($activities_template->activities,true)."\n-_-_-__-\n";
			foreach($activities_template->activities as $act){

				if(in_array($act->type,$ALLOWED_ACTIVITIES)){
					$id = $act->id;
					$feed[$count]['activity_id'] = $id;
					$feed[$count]['avatar'] = bp_core_fetch_avatar(array('html'=>false, 'item_id'=>$id));
					$feed[$count]['comment'] = $act->content;
					$feed[$count]['display_name'] = $act->display_name;
					$feed[$count]['date_recorded'] = date("F j, Y, g:i a",strtotime($act->date_recorded));
					if(isset($act->children)){
						$feed[$count]['comment_count'] = count($act->children);
					}
					$count++;
				}

				// TODO test this will exit the loop
				if($count == $limit){
					break;
				}
			}
		endif;
		$json = json_encode($feed);
		if($json){
			return $json ;
		}

		return $feed;
	}

	/**
	 * Add an Item to the BuddyPress Activity Feed.
	 *
	 * @TODO : The call to bp_activity_add() fires the Pusher and will probably timeout the RPC call so it will not return
	 *
	 * @param
	 *
	 * @return BOOL $added true id the item was added
	 */
	public static function addActivity ($args) {

		$user = $args[0];
		$pass = $args[1];
		$comment = $args[2];
		// This is for items that are to be assosiated as a comment
		if(isset($args[3])){
			$addToId = (int)$args[3];
		}

		// authenticate the user
		$isUser = Authenticate::isValidUser($user,$pass);

		if(get_class($isUser) == "IXR_Error"){
			return $isUser;
		}

		// add their item
		$newItem = array();
		$newItem['user_id']		= $isUser->data->ID;
		$newItem['content']		= $comment;
		$newItem['action']		= $user." posted remotely";
		$newItem['component']	= 'activity';
		$newItem['type']		= 'activity_update';
		// This is for items that are to be assosiated as a comment
		if(isset($addToId)){
			$newItem['item_id']	= $addToId;
			$newItem['type']	= 'activity_comment';
			$newItem['secondary_item_id']	= $addToId;
		}

		if (!function_exists('bp_activity_add')){
			return json_encode(array ('Error' => 'NO BUDDYPRESS'));
		}
		$insertId = bp_activity_add($newItem);
		//@TODO - stalls here on testing, making the RPC call time out
		if($insertId){
			$postInsert = array ('error_code'=>0);//        'id' => $insertId);
		}else{
			$postInsert = array ('error_code'=>1); ///('ERROR' => 'failed to add item');
		}
		return json_encode($postInsert);
	}

	/**
	 * Add an Item to the BuddyPress Activity Feed.
	 *
	 * @param
	 *
	 * @return String $i JSON id if the item was added, or error on Fail
	 */
	public static function addComment ($args) {
		return XMLRPCExtend::addActivity($args);
	}

	/**
	 * Get the URL for an Avatar.
	 *
	 * @param int $userId The user Id to get the avatar for, or empty if default avatar required
	 *
	 * @return String $url A url location of an avatar
	 */
	public static function getAvatarURL ($userId = false) {
		$url = '';
		return $userId;
	}

	/**
	 * Get an Avatar.
	 *
	 * @param int $userId The user Id to get the avatar for, or empty if default avatar required
	 *
	 * @return Image/* $image The binary icon
	 */
	public static function getAvatar ($userId = false) {
		$image = '';
		return $userId;
	}

	public static function androidTest(){
		return "Hello Android - Welcome!";
	}
}

?>