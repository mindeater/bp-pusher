<?php

/**
 * Provides the Pusher_Authenticate class.
 *
 * @package    RayWhitePushNotifications
 */
/**
 * Authenticates Users.
 *
 * @package    RayWhitePushNotifications
 * @subpackage Pusher
 */

if(!class_exists("Authenticate")) {

	class Authenticate{

		function Authenticate(){

		}

		function isValidUser($user,$pass){

			// authenticate the user via WordPress
			$user = wp_authenticate($user, $pass);

			if (is_wp_error($user)) {
				//$this->error = new IXR_Error(403, __('Bad login/pass combination.'));
				return new IXR_Error(403, __('Bad login/pass combination.'));
			}

			return $user;
		}
	}

}

?>