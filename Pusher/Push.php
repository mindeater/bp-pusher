<?php

/**
 * Provides the Pusher_Push class.
 *
 * @package    RayWhitePushNotifications
 */
/**
 * Provides some standard functionality related to users.
 *
 * @package    RayWhitePushNotifications
 * @subpackage Pusher
 */
if (!class_exists("Push")) {

	class Push {

		var $activityItem;
		var $payload;

		var $CHAR_LIMIT = 100; // probably 190 will work
		var $passPhrase = 'iceiceB@by2012';//'8uddypr3ss';//'iceicebaby2011*3';
		var $pemFile;
		var $mode = 'deploy';//development';
		var $server = array(
							'development'	=>'ssl://gateway.sandbox.push.apple.com:2195',
							'deploy'		=>'ssl://gateway.push.apple.com:2195',
							'feedback'		=>'ssl://feedback.push.apple.com:2196'
							);

		function Push () {
			$this->pemFile = dirname(__FILE__).'/ck.pem';
		}

		function setActivity($args) {
			$this->activityItem = $args;
			//echo '<pre>'.print_r($args).'</pre>';
			$this->setUpPackage();

			//echo '<pre>' . print_r($args, true) . '</pre>';
			/* standard activity item
			  [id] =>
			  [action] => admin posted an update
			  [content] => added an item to the activity stream
			  [component] => activity
			  [type] => activity_update
			  [primary_link] => http://xampp.dev/wordpress/members/admin/
			  [user_id] => 1
			  [item_id] =>
			  [secondary_item_id] =>
			  [recorded_time] => 2012-04-04 02:02:44
			  [hide_sitewide] =>
			 * added to another item
			  [id] =>
			  [action] => admin posted a new activity comment
			  [content] => addedd to another activity item
			  [component] => activity
			  [type] => activity_comment
			  [primary_link] =>
			  [user_id] => 1
			  [item_id] => 15
			  [secondary_item_id] => 15
			  [recorded_time] => 2012-04-04 02:04:30
			  [hide_sitewide] => 0
			 */
		}

		/**
		 * Creates the message payload trimming the Activity Item to fit the Apple Size Limit
		 */
		function setUpPackage(){
			// Create the payload body
			$user_info = get_userdata($this->activityItem['user_id']);
			$username = $user_info->user_login;
			$adjective = ' wrote ';
			if($this->activityItem['type'] == 'activity_comment'){
				$adjective = ' commented ';
			}
			$message = substr($username.$adjective.$this->activityItem['content'],0,$this->CHAR_LIMIT)."...";
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default'
				);

			// Encode the payload as JSON
			$this->payload = json_encode($body);

		}



		//
		///// ANDROID MODS
		// http://stackoverflow.com/questions/11242743/gcm-with-php-google-cloud-messaging
		////////////////
		//

		/**
		 * This notify Method makes a raw connnection to the Apple Push Notification Service
		 *
		 * @TODO: This should use a message Queue to prevent blocking the Web Server / XML-RPC Thread
		 */
		function notify () {
			if(!isset($this->activityItem)) return;
			global $wpdb;
			// get the list and loop them sending notifications to each
			$sql = 'SELECT * FROM '.$wpdb->prefix.'pusher_devices WHERE should_get_push >0 AND wp_user_id NOT IN ("'.$this->activityItem['user_id'].'")';
			$devices = $wpdb->get_results($sql,ARRAY_A);

			if(!empty($devices)){
				////////////////////////////////////////////////////
				// make the connection
				// Only one connection should be made to Apple Server
				$ctx = stream_context_create();
				// get the location of the pem file
				stream_context_set_option($ctx, 'ssl', 'local_cert', $this->pemFile);
				stream_context_set_option($ctx, 'ssl', 'passphrase', $this->passPhrase);

				// Open a connection to the APNS server
				$fp = stream_socket_client($this->server[$this->mode], $err,
					$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
				$fp2 = fopen("_stream_log.txt", "w+");

				foreach($devices as $device){
					// Build the binary notification
					$msg = chr(0) . pack('n', 32) . pack('H*', $device['pusher_deviceToken']) . pack('n', strlen($this->payload)) . $this->payload;
					// Send it to the server
					$result = fwrite($fp, $msg, strlen($msg));

					fwrite($fp2,$device['pusher_deviceToken'],strlen($device['pusher_deviceToken']));
					fwrite($fp2,$msg,strlen($msg));

				}

				fclose($fp2);
				fclose($fp);
			}

		}


		function notifySingleDevice($deviceId,$message) {

			$body['aps'] = array(
				'alert' => substr($message,0,$this->CHAR_LIMIT),
				'sound' => 'default'
				);

			// Encode the payload as JSON
			$this->payload = json_encode($body);

			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $this->pemFile);
			stream_context_set_option($ctx, 'ssl', 'passphrase', $this->passPhrase);

			// Open a connection to the APNS server
			echo ' connecting to '.$this->server[$this->mode];

			$fp = stream_socket_client($this->server[$this->mode], $err,
				$errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceId) . pack('n', strlen($this->payload)) . $this->payload;

			$result = fwrite($fp, $msg, strlen($msg));

			echo " result=".$result." err=".$err." errstr=".$errstr;
			fclose($fp);

		}


		function notifyMultipleDevices($devices, $message){
			$body['aps'] = array(
				'alert' => substr($message,0,$this->CHAR_LIMIT),
				'sound' => 'default'
				);

			// Encode the payload as JSON
			$this->payload = json_encode($body);

			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $this->pemFile);
			stream_context_set_option($ctx, 'ssl', 'passphrase', $this->passPhrase);

			$fp = stream_socket_client($this->server[$this->mode], $err,
				$errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

			foreach($devices as $deviceId){
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceId) . pack('n', strlen($this->payload)) . $this->payload;

				$result = fwrite($fp, $msg, strlen($msg));
			}

			fclose($fp);
			return '<pre>'.print_r($devices,true).'</pre>';
		}

		/*
		 * Direct rip from http://code.google.com/p/apns-php/
		 * Feedback.php
		 */
		function retriveFeedback(){

			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $this->pemFile);
			stream_context_set_option($ctx, 'ssl', 'passphrase', $this->passPhrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client($this->server['feedback'], $err,
				$errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
			//$feedback = stream_get_contents($fp);
			////////////////////////////////////////////
			//const TIME_BINARY_SIZE = 4; /**< @type integer Timestamp binary size in bytes. */
			//const TOKEN_LENGTH_BINARY_SIZE = 2; /**< @type integer Token length binary size in bytes. */
			//const DEVICE_BINARY_SIZE = 32; /**< @type integer Device token length. */
			$nFeedbackTupleLen = 4 + 2 + 32;
			$this->_aFeedback = array();
			$sBuffer = '';
			$_log = '';
			while (!feof($fp)) {
				$_log.= 'INFO: Reading...'."\n";

				$sBuffer .= $sCurrBuffer = fread($fp, 8192);
				$nCurrBufferLen = strlen($sCurrBuffer);
				if ($nCurrBufferLen > 0) {
					$_log.= "INFO: {$nCurrBufferLen} bytes read.\n";
				}
				unset($sCurrBuffer, $nCurrBufferLen);

				$nBufferLen = strlen($sBuffer);
				if ($nBufferLen >= $nFeedbackTupleLen) {
					$nFeedbackTuples = floor($nBufferLen / $nFeedbackTupleLen);
					for ($i = 0; $i < $nFeedbackTuples; $i++) {
						$sFeedbackTuple = substr($sBuffer, 0, $nFeedbackTupleLen);
						$sBuffer = substr($sBuffer, $nFeedbackTupleLen);
						$this->_aFeedback[] = $aFeedback = unpack('Ntimestamp/ntokenLength/H*deviceToken', $sFeedbackTuple);
						$_log .= sprintf("INFO: New feedback tuple: timestamp=%d (%s), tokenLength=%d, deviceToken=%s.\n",
							$aFeedback['timestamp'], date('Y-m-d H:i:s', $aFeedback['timestamp']),
							$aFeedback['tokenLength'], $aFeedback['deviceToken']
						);
						unset($aFeedback);
					}
				}

				$read = array($fp);
				$null = NULL;
				$nChangedStreams = stream_select($read, $null, $null, 0, 120);
				if ($nChangedStreams === false) {
					$_log .='WARNING: Unable to wait for a stream availability.';
					break;
				}
			}
			fclose($fp);
			return print_r($this->_aFeedback,true).'|_log|'.$_log;


			/////////////////////////////////////////////

		}

	}

}
?>
