<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script>
	jQuery(document).ready(function(){
		jQuery(".test-push").click(function(event) {
			event.preventDefault();
			var isOK = confirm("Send this device a Push ?");
			if(isOK){
				var mess = jQuery("#MESSAGE").val();
				if(mess.length == 0){
					alert("Need Message Body");
					return;
				}

				var data = {
					action: 'pusher_js_send_admin_push',
					device: jQuery(this).parent().siblings(".deviceId")[0].innerHTML,
					message: mess,
					token: 'hf'
				};

				jQuery.post(ajaxurl, data, function(response) {
					alert('Got this from the server: ' + response);
					if(response){
					//alert(jQuery('select#s-select'));
						jQuery('#message').append(response);
						jQuery('#message').show();
						}
				});

			}else{

			}
		});

		jQuery(".retrive-feedback").click(function(event) {
			event.preventDefault();
			var isOK = confirm("Get Feedback from Apple?");
			if(isOK){

				var data = {
					action: 'pusher_js_retrive_feedback_admin_push',
					token: 'hf'
				};

				jQuery.post(ajaxurl, data, function(response) {
					alert('Got this from the server: ' + response);
					if(response){
					//alert(jQuery('select#s-select'));
						jQuery('#message').append(response);
						jQuery('#message').show();
						}
				});

			}else{

			}
		});

		jQuery(".push2selected").click(function(event) {
			event.preventDefault();
			var devices = [];
			jQuery("input[type=checkbox].pusher-send").each(function(){
				if(this.checked){
					devices.push(jQuery(this).parent().siblings(".deviceId")[0].innerHTML);
				}
			});

			if(devices.length == 0){
				alert("Please select a device to push to.");
				return;
			}

			var isOK = confirm("Push to Selected?");
			if(isOK){
				var mess = jQuery("#MESSAGE").val();

				if(mess.length == 0){
					alert("Need Message Body");
					return;
				}

				var data = {
					action: 'pusher_js_send_multiple_admin_push',
					deviceList: devices,
					message: mess,
					token: 'hf'
				};

				jQuery.post(ajaxurl, data, function(response) {
					alert('Got this from the server: ' + response);
					if(response){
					//alert(jQuery('select#s-select'));
						jQuery('#message').append(response);
						jQuery('#message').show();
						}
				});

			}else{

			}
		});
	});
</script>
<style>
	.status-Yes{
		color:#006600;
		font-weight: bold;
	}
	.status-No{
		color:#cc0000;
		font-weight: bold;
	}
</style>
<div id="bp_pusher_admin_page">
	<h2>BuddyPress Pusher</h2>
	<?php
	global $wpdb;
	if(isset($_GET['toggle'])){
		check_admin_referer('pusher_switch_notification');
		$query = 'UPDATE '.$wpdb->prefix.'pusher_devices SET should_get_push = !should_get_push WHERE pusher_table_id = '.$_GET['toggle'];
		$wpdb->get_results($query);
		?>
		<div id="message" class="updated below-h2"> Device Updated</div>
	<?php
	}

	if(isset($_GET['remove'])){
		check_admin_referer('pusher_remove_device');
		$query = 'DELETE FROM '.$wpdb->prefix.'pusher_devices WHERE pusher_table_id = '.$_GET['remove'];
		$wpdb->get_results($query);
		?>
		<div id="message" class="updated below-h2"> Device Updated</div>
	<?php
	}

	?>

	<div id="message" class="updated below-h2" style="display:none"></div>
	<pre>
	Options relating to the Custom Plugin.
	1. Development or Deployment
	2. Dev certificate
	2.1 Dev certificate passphrase
	3. Deploy certificate
	3.1 Deploy certificate passphrase
	4. Broadcast
	5. Send to Device
	6. device/user list
	</pre>
	<div>
		<hr />
		<table class="widefat">
			<thead>
				<tr>
					<th>Test Message</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th></th>
				</tr>
			</tfoot>
			<tbody>
				<tr>
					<td><input type="text" name="bp_pusher_TESTMESSAGE" id="MESSAGE" value="Admin Test Message">

						<a href="#" class="push2all button">Push to All</a>
						<a href="#" class="retrive-feedback button">Retrive Feedback</a>
					</td>
				</tr>
			</tbody>
		</table>

		<h3>Devices</h3>
		<table class="widefat">
			<thead>
			<tr>
				<th colspan="2"><a href="#" class="push2selected button">Push to Selected</a></th>
				<th>Get Push</th>
				<th>User Name</th>
				<th>Device Token</th>
				<th>Last Access</th>
				<th> - </th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<th colspan="2"><a href="#" class="push2selected button">Push to Selected</a></th>
				<th>Get Push</th>
				<th>User Name</th>
				<th>Device Token</th>
				<th>Last Access</th>
				<th> - </th>
			</tr>
			</foot>
			<tbody>
		<?php

		$sql = 'SELECT '.$wpdb->prefix.'pusher_devices.*, '.
				$wpdb->prefix.'users.*,
				COUNT('.$wpdb->prefix.'bp_activity.user_id) as count FROM '.
				$wpdb->prefix.'users, '.$wpdb->prefix.'bp_activity, '.
				$wpdb->prefix.'pusher_devices WHERE '.
				$wpdb->prefix.'pusher_devices.wp_user_id = '.$wpdb->prefix.'users.ID AND '.
				$wpdb->prefix.'users.ID = '.$wpdb->prefix.'bp_activity.user_id';
		$sql = 'SELECT * FROM '.
				$wpdb->prefix.'users, '.
				$wpdb->prefix.'pusher_devices WHERE '.
				$wpdb->prefix.'pusher_devices.wp_user_id = '.$wpdb->prefix.'users.ID '.
				'ORDER BY '.$wpdb->prefix.'pusher_devices.pusher_table_id';
		$devices = $wpdb->get_results($sql,ARRAY_A);
		foreach($devices as $device){
			//echo '<li>'.print_r($device,true).'</li>';
			echo '<tr id="post-'.$device['pusher_table_id'].'" class="post">'."\n";
			echo '<td><input type="checkbox" class="pusher-send" /></td>';
			echo '<td><a href="#" class="test-push">Test Push</a></td>';
			$status = $device['should_get_push'] ? 'Yes' : 'No';
			echo '<td><span class="status-'.$status.'">'.$status.'</span></td>';

			echo '<td>';
			echo '<span class="row-title">'.$device['user_nicename'].' &lt;'.$device['user_email'].'&gt;</span>';
			echo '<div class="row-actions>';
			echo '<span class="trash"><a href="#">Trash</a></span> | ';

			$link = 'options-general.php?page=bp-pusher&toggle='.$device['pusher_table_id'];
			$link =  wp_nonce_url($link, 'pusher_switch_notification');

			echo '<span><a href="'.$link.'">Toggle Notifications</a></span>';
			echo '</div>';

			echo '</td>';
			echo '<td style="font-family:monospace" class="deviceId">'.$device['pusher_deviceToken'].'</td>';


			echo '<td>'.$device['last_access_date'].'</td>';

			echo '<td> --- </td>';
			// [pusher_deviceToken] => XYXYXYXYXYXYXYXYIILLLLLLL [last_access_date] => 0000-00-00 [should_get_push] => 1 [wp_user_id] => 1 [ID] => 1 [user_login] => admin [user_pass] => $P$BN1ichgrK8omh21NtHCv8F8cAJg4oD. [user_nicename] => admin [user_email] => ashley@mindeater.com [user_url] => [user_registered] => 2011-08-28 11:10:33 [user_activation_key] => [user_status] => 0 [display_name]
			echo '</tr>'."\n";
		}
		?>
			<tbody>
		</table>
	</div>
<?php echo $sql; ?>
	<form action="options.php" method="post">
		<?php settings_fields('plugin_options'); ?>
		<?php do_settings_sections('plugin'); ?>

		<input name="Submit" type="submit" value="<?php esc_attr_e('Save Changes'); ?>" />
	</form>
</div>
