<?php
/*
Plugin Name:    BuddyPress iOS Pusher
Plugin URI:     http://mindeater.com
Description:    Creates XML-RPC methods for registered users to collect recieve BuddyPress Activity feed from POST calls and to add items to the Activity feed. When configured these items are sent to Apple Push Notification Service.
Version:        1.0
Author:         Ashley McCoy
Author URI:     http://mindeater.com
 */

//@TODO - use the specialize Json encode function instaed of php's

/* Constants */

/* Files */
require_once 'Pusher/Timer.php';
require_once 'Pusher/Authenticate.php';
require_once 'Pusher/Push.php';
require_once 'Pusher/XMLRPCExtend.php';
require_once 'Pusher/Plugin.php';



/* Runtime */
if (class_exists("Plugin")) {
	$rwPusherPluginInstance = new Plugin();

	/* activation hook for database table set up */
	register_activation_hook(__file__, array($rwPusherPluginInstance,'install'));
}

/* helpers */

// We don't use PHP's built-in json_encode() function because it converts
// UTF-8 characters to \uxxxx. That eats up 6 characters in the payload for
// no good reason, as JSON already supports UTF-8 just fine.
function jsonEncode($text)
{
	static $from = array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"');
	static $to = array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"');
	return str_replace($from, $to, $text);
}